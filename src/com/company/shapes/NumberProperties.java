package com.company.shapes;

import javafx.scene.control.SplitPane;

public class NumberProperties {

    protected boolean isDividedInto(int z, int y) {

        float fNumber = (float) z / y;
        z = (int) fNumber;
        return z == fNumber;
    }

    protected boolean checkInputArray(int[] arr) {
        if  ( arr == null ) {
            System.out.println("Array is null");
            return false;
        }

        if ( arr.length == 0 ) {
            System.out.println("Length of array = 0");
            return false;
        }

        return true;
    }


    protected boolean[] calculateEven(int[] a) {

        if ( ! checkInputArray(a) ) {
            return null;
        }

        boolean[] b = new boolean[a.length];

        for (int i = 0; i < a.length; i++) {
            b[i] = isDividedInto(a[i],2 );
        }

        return b;
    }

    protected boolean isDividedIntoPair(int x, int y, int z) {
        float fNumber;
        int iNumber;
        boolean bDivide = false;

        if ( (y == 0) || (z == 0)) {
            return false;
        }

        fNumber = (float) x/y;
        iNumber = (int) fNumber;

        if ( iNumber == fNumber) {
            bDivide = true;
        }

        if ( bDivide  ) {
            bDivide = false;

            fNumber = (float) x/z;
            iNumber = (int) fNumber;

            if ( iNumber == fNumber) {
                bDivide = true;
            }

        } else {
            return false;
        }

        return bDivide;
    }

    protected boolean[] calculateRepeatNumber(int[] a) {

        if ( ! checkInputArray(a) ) {
            return null;
        }

        if ( a.length == 0) {
            return null;
        }

        boolean[] b = new boolean[a.length];
        char[] c = new char[3];
        String sNumber;


        for (int i = 0; i < a.length; i++) {
            sNumber = String.valueOf(a[i]);
            if ( sNumber.length() == 3 ){
                c = sNumber.toCharArray();
                if ( c[0] == c[1] || c[0] == c[2] || c[1] == c[2] ) {
                    b[i] = false;
                } else {
                    b[i] = true;
                }
            } else {
                b[i] = false;
            }

        }

        return b;
    }

    protected boolean[] calculateDivideInto(int[] a, int t, int p) {

        if ( ! checkInputArray(a) ) {
            return null;
        }

        boolean[] b = new boolean[a.length];

        for (int i = 0; i < a.length; i++) {
            b[i] = isDividedIntoPair(a[i], t, p);
        }

        return b;
    }
}
