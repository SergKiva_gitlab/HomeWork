package com.company.shapes;

public class StrArray {
    private int arraySize;
    private String[] array;
    private int[] arrayOfLength;
    private int maxLength;
    private int minLength;

    public StrArray(int arraySize) {
        this.arraySize = arraySize;
        arrayOfLength = new int[arraySize];
        array = new String[arraySize];
        maxLength = 0;
        minLength = 0;
    }

    public int getArraySize() {
        return arraySize;
    }

    public void setArrayElement(String value, int index) {

        this.array[index] = value;
        arrayOfLength[index] = value.length();

        if ( maxLength <  value.length()) {
            maxLength = value.length();
        }

        if ( index == 0 ) {
            minLength = value.length();
        } else {
            if ( minLength > value.length() ) {
                minLength = value.length();
            }

        }

    }

    public void printInputArrayAndMinMax() {

        System.out.println("Input array of string");
        for (int i = 0; i < getArraySize(); i++) {
            System.out.println("(length=" + array[i].length() + ") " + i + ": " + array[i]);
        }

        System.out.println("The shortest (=" + minLength + "):");
        for (int i = 0; i < getArraySize(); i++) {
            if ( minLength == array[i].length() ) {
                System.out.println(array[i]);
            }
        }

        System.out.println("The longest (=" + maxLength + "):");
        for (int i = 0; i < getArraySize(); i++) {
            if ( maxLength == array[i].length() ) {
                System.out.println(array[i]);
            }
        }
    }

    private void checkPair(int a, int b) {
        if ( array[a].length() > array[b].length() ) {
            String x = array[b];
            array[b] = array[a];
            array[a] = x;
        }
    }

    private boolean sortStrArray(String sDirection) {
        if ( getArraySize() < 2 ) {
            System.err.println("Array size is not correct.");
            return false;
        }

        if (sDirection == "DOWN") {

            for (int i=0; i < getArraySize()-1; i++) {

                for (int j=getArraySize()-1; j > i; j--) {
                    checkPair(j, j-1);
                }
            }

        } else {
            for (int i=getArraySize() - 1; i > -1 ; i--) {

                for (int j=0; j < i; j++) {
                    checkPair(j,j+1);
                }
            }
        }

        return true;
    }

    public void printSortedStr() {
        int i;
        sortStrArray("UP");
        System.out.println("Ascending:");
        for ( i = 0; i < getArraySize(); i++ ) {
            System.out.println(array[i]);
        }
        System.out.println("");
        System.out.println("Descending:");
        for ( i = getArraySize() - 1; i > -1 ; i-- ) {
            System.out.println(array[i]);
        }
        System.out.println("");
    }

    private int getMediumLength() {
        int sum = 0;
        for ( int i = 0; i < getArraySize(); i++ ) {
            sum = sum + array[i].length();
        }

        final int i = (int) sum / getArraySize();
        return i;
    }

    public void printTask3() {
        int medium = getMediumLength();
        int i;

        System.out.println("More than medium length(=" + medium + "):");
        for ( i = 0; i < getArraySize(); i++ ) {
            if ( array[i].length() > medium ) {
                System.out.println("length = " + array[i].length() + ": " + array[i]);
            }

        }

        System.out.println("Less than medium length(=" + medium + "):");
        for ( i = 0; i < getArraySize(); i++ ) {
            if ( array[i].length() < medium ) {
                System.out.println("length = " + array[i].length() + ": " + array[i]);
            }

        }
    }

}
