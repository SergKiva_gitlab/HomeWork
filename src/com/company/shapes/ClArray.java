package com.company.shapes;

public class ClArray {
    private int arrSize;
    private int[] array;

    public ClArray(int arrSize) {
        this.array = new int[arrSize];
        this.arrSize = arrSize;
    }

    public ClArray(String[] sArgs) {
        if ( sArgs == null || sArgs.length == 0 ) {
            System.out.println("Not found array in arguments of command line");
            return;
        }

        array = new int[sArgs.length];
        for (int i=0; i < sArgs.length; i++) {
            try {
                array[i] = Integer.parseInt(sArgs[i]);
            } catch(NumberFormatException nfe) {
                array[i] = (int) (Math.random() * 11 ) + 10;;
                System.err.println("Invalid Format! - " + sArgs[i] + ". Set random value = " + array[i]);
            }

        }
        arrSize = array.length;
        System.out.println("Found array in arguments of command line. Count of elements: " + arrSize);
    }


    public int getArrSize() {

        return arrSize;
    }

    public void setElementOfArray(int num, int value){
        if (num < this.getArrSize() ) {
            array[num] = value;
        } else {
            System.err.println("Index " + num + "not in array");
        }
    }
    /*
        private void checkPair(int a, int b) {
            if ( array[a] > array[b] ) {
                int x = array[b];
                array[b] = array[a];
                array[a] = x;
            }
        }

        public boolean sortClArray(String sDirection) {
            if ( getArrSize() < 2 ) {
                System.err.println("Array size is not correct.");
                return false;
            }

            if (sDirection == "DOWN") {

                for (int i=0; i < getArrSize()-1; i++) {

                    for (int j=getArrSize()-1; j > i; j--) {
                        checkPair(j, j-1);
                    }
                }

            } else {
                for (int i=getArrSize() - 1; i > -1 ; i--) {

                    for (int j=0; j < i; j++) {
                        checkPair(j,j+1);
                    }
                }
            }

            return true;
        }
    */
    public void printArray() {
        for ( int i = 0; i < getArrSize(); i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }


    public void printEvenAndOdd() {
        int j = 0;
        boolean[] bIsEven = new boolean[getArrSize()];

        NumberProperties np = new NumberProperties();
        bIsEven = np.calculateEven(array);

        System.out.println("Even numbers:");
        for ( int i = 0; i < getArrSize(); i++) {
            if ( bIsEven[i] ) {
                System.out.print(array[i] + " ");
                j = j + 1;
            }
        }

        if ( j == 0 ) {
            System.out.println("No even numbers.");
        } else {
            System.out.println("");
            j = 0;
        }

        System.out.println("Odd numbers:");
        for ( int i = 0; i < getArrSize(); i++) {
            if ( ! bIsEven[i] ) {
                System.out.print(array[i] + " ");
                j = j + 1;
            }
        }

        if ( j == 0 ) {
            System.out.println("No odd numbers.");
        } else {
            System.out.println("");
            j = 0;
        }

    }


    public void printDividedInto(int z1, int z2) {
        int j = 0;
        boolean[] bIsDivided = new boolean[getArrSize()];

        NumberProperties np = new NumberProperties();
        bIsDivided = np.calculateDivideInto(array, z1, z2);

        System.out.println(" Are divided into (" + z1 + " and " + z2 + "):");
        for ( int i = 0; i < getArrSize(); i++) {
            if ( bIsDivided[i] ) {
                System.out.print(array[i] + " ");
                j = j + 1;
            }
        }

        if ( j == 0 ) {
            System.out.println("No numbers.");
        } else {
            System.out.println("");
            j = 0;
        }

        System.out.println(" Not divisible by (" + z1 + " and " + z2 + "):");
        for ( int i = 0; i < getArrSize(); i++) {
            if ( ! bIsDivided[i] ) {
                System.out.print(array[i] + " ");
                j = j + 1;
            }
        }

        if ( j == 0 ) {
            System.out.println("No numbers.");
        } else {
            System.out.println("");
        }

    }

    public void printNotRepeated() {
        int j = 0;
        boolean[] bIsRepeated = new boolean[getArrSize()];

        NumberProperties np = new NumberProperties();
        bIsRepeated = np.calculateRepeatNumber(array);

        System.out.println(" String length = 3 and does not have duplicate numbers:");
        for ( int i = 0; i < getArrSize(); i++) {
            if ( bIsRepeated[i] ) {
                System.out.print(array[i] + " ");
                j = j + 1;
            }
        }

        if ( j == 0 ) {
            System.out.println("No numbers.");
        } else {
            System.out.println("");
        }

    }
}
