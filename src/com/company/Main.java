package com.company;

import com.company.shapes.ClArray;
import com.company.shapes.ReadValue;
import com.company.shapes.StrArray;

import java.io.IOException;

public class Main {

    public static void main(String[] args)  throws IOException {
        int iTmp;

        ClArray arrayOfInt = new ClArray(args);
        ReadValue inConsole = new ReadValue();
        if (arrayOfInt == null || arrayOfInt.getArrSize() == 0) {


            iTmp = inConsole.getReadedNumber("Enter array size:");
            if (iTmp < 2) {
                System.out.println("Array size (="  + iTmp  +") too small");
                return;
            }

            arrayOfInt = new ClArray(iTmp);
            for (int i = 0; i < arrayOfInt.getArrSize(); i++) {
                iTmp = inConsole.getReadedNumber("Enter array element " + i + ":");
                arrayOfInt.setElementOfArray(i, iTmp);
            }
        }

        System.out.println("Input array:");
        arrayOfInt.printArray();

        arrayOfInt.printEvenAndOdd();

        arrayOfInt.printDividedInto(3, 9);
        arrayOfInt.printDividedInto(5, 7);

        arrayOfInt.printNotRepeated();

        /*  задача по строкам */

        iTmp = inConsole.getReadedNumber("Enter size (array of string):");
        if (iTmp < 2) {
            System.out.println("Array size (="  + iTmp  +") too small");
            return;
        }

        StrArray arrayOfStr = new StrArray(iTmp);

        for (int i = 0; i < arrayOfStr.getArraySize(); i++) {
            arrayOfStr.setArrayElement(inConsole.getInputString("Enter array element " + i + ":"), i);
        }

        System.out.println("");
        arrayOfStr.printInputArrayAndMinMax();
        System.out.println("");
        arrayOfStr.printSortedStr();
        System.out.println("");
        arrayOfStr.printTask3();


/*
        System.out.println("Input array:");
        arrayOfInt.printArray();

        arrayOfInt.sortClArray("DOWN");

        System.out.println("Output array, descending sorting:");
        arrayOfInt.printArray();

        arrayOfInt.sortClArray("UP");
        System.out.println("Output array, sort ascending:");
        arrayOfInt.printArray();
*/
    }

}
